
from django.contrib import admin
from django.urls import include, path
from accounts import views
from accounts.forms import EmailAuthenticationForm
from django.contrib.auth.views import LoginView

app_name = 'upload_qpj'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('quizzes.urls')),
    path('quizzes/', include('quizzes.urls')),
    path('login/', LoginView.as_view(form_class=EmailAuthenticationForm,
                                     template_name='registration/index.html'), name='login'),
    path('logout/', views.Logout.as_view(), name='logout'),
    path('accounts/', views.UserCreate.as_view(), name='user_create'),
    path('accounts/done/', views.UserCreateDone.as_view(), name='user_create_done'),
    path('accounts/complete/<token>/', views.UserCreateComplete.as_view(), name='user_create_complete'),
    path('accounts/password_change/', views.PasswordChange.as_view(), name='password_change'),
    path('accounts/password_change/done/', views.PasswordChangeDone.as_view(), name='password_change_done'),
    path('accounts/password_reset/', views.PasswordReset.as_view(), name='password_reset'),
    path('accounts/password_reset/done/', views.PasswordResetDone.as_view(), name='password_reset_done'),
    path('accounts/reset/<uidb64>/<token>/', views.PasswordResetConfirm.as_view(), name='password_reset_confirm'),
    path('accounts/reset/done/', views.PasswordResetComplete.as_view(), name='password_reset_complete'),
    path('sysconfig/', include('system_conf.urls')),
]
