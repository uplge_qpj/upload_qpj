from django.db import models
from accounts.models import User


class CategoryMaster(models.Model):
    """カテゴリトラン"""
    class Meta:
        db_table = 'quizzes_category_master'

    """カテゴリ名マスタ"""
    name = models.CharField(max_length=50)


class QuestionTypeMaster(models.Model):
    """カテゴリトラン"""
    class Meta:
        db_table = 'quizzes_question_type_master'

    """解答形式マスタ"""
    """問題タイプ　ラジオボタン　テキストボックス　チェックボックス"""
    type_name = models.CharField(max_length=50)
    type_dom = models.CharField(max_length=300)


class WorkbookCategory(models.Model):
    """カテゴリトラン"""
    class Meta:
        db_table = 'quizzes_workbook_category'

    category = models.OneToOneField(CategoryMaster, on_delete=models.CASCADE)


class Workbook(models.Model):
    """問題集トラン"""
    workbook_category = models.ForeignKey(WorkbookCategory, on_delete=models.CASCADE)
    title = models.CharField(max_length=300)
    workbook_description = models.TextField()
    last_create_date = models.TimeField()


class Question(models.Model):
    """問題内容テーブル"""
    workbook = models.ForeignKey(Workbook, on_delete=models.CASCADE)
    question = models.TextField()
    """問題タイプ　ラジオボタン　テキストボックス　チェックボックス"""
    question_type = models.ForeignKey(QuestionTypeMaster, on_delete=models.DO_NOTHING)
    correct_answer = models.CharField(max_length=100)
    commentary = models.TextField()
    point_allocation = models.IntegerField(default=0)


class Choice(models.Model):
    """選択肢テーブル"""
    question_id = models.ForeignKey(Question, on_delete=models.CASCADE)
    option = models.CharField(max_length=100) # 回答選択肢


class Progress(models.Model):
    """解答済み問題集トラン"""
    user_id = models.ForeignKey(User, on_delete=models.CASCADE)
    workbook = models.ForeignKey(Workbook, on_delete=models.CASCADE)
    progress_per = models.IntegerField(default=0)
    last_answer_date = models.TimeField()


class Answered(models.Model):
    """ユーザー毎解答トラン"""
    progress = models.ForeignKey(Progress, on_delete=models.CASCADE)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    answered = models.CharField(max_length=100)
