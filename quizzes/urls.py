from django.urls import path

from . import views

app_name = 'quizzes'

urlpatterns = [
    path('', views.Top.as_view(), name='top'),
    path('quizzes/', views.make_question, name='make_question'),
]
