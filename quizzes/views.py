from django.shortcuts import render
from django.views import generic
from django.views.generic import ListView
from quizzes.models import Workbook, Question


def make_question(request):
    return render(request, 'make_question/make_question.html')


class Top(ListView):
    model = Workbook
    template_name = "quizzes/main_page.html"
