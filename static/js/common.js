window.addEventListener('load',function(){

    Vue.component('logo-content', {
        template: '<div id="header-logo"><a href="main.html"><span>UPLGE E-learning</span></a></div>'
    });

    var headerContent = Vue.extend({
        template: '<div class="header-content"><span id="burger-menu"></span><ul><li><a href="register.html">Register</a></li><li><a href="login.html">Login</a></li></ul></div>'
    });
    // header new Vue
    var head = new Vue({
        el: "header",
        components: {
            'header-content': headerContent
        },
        data: {
            message: "hello"
        }
    });

    // footer components
    var linksContent = Vue.extend({
      template: '<ul class="links"><li><a class="arrow-right" href="https://www.upload-gp.co.jp/" target="_blank">UPLOAD 公式HP</a></li><li><a class="arrow-right" href="https://www.upload-gp.co.jp/privacy.html" target="_blank">プライバシーポリシー</a></li><li><a class="arrow-right" href="https://www.upload-gp.co.jp/outline.html" target="_blank">会社概要</a></li><li><a class="arrow-right" href="https://www.upload-gp.co.jp/outline.html#contact" target="_blank">お問い合わせ</a></li></ul>'
    });
    var footerContent = Vue.extend({
      template: '<div class="footer-content"><links-content></links-content></div>',
      components: {
        'links-content': linksContent
      }
    });
    var foot = new Vue({
      el: "footer",
      components: {
        'footer-content': footerContent
      }
    });

},false);
