var QUIZ = QUIZ || {}
QUIZ.homeComponent = {
    props:['kind', 'title'],
    data: function () {
        return {
            isDetailShow: false
            , detailText: '+'
            , image: function()
            {
                console.log('quiz-content-' + this.title);
                return 'quiz-content-' + this.title;
            }
        }
    },
    template: '<div class="content-box">' +
        '<div class="title" @click="detailShow">{{ title }}</div>' +
        '<div class="togle" @click="detailShow">{{ detailText }}</div>' +
        '<div class="quiz-area" :class="{ \'quiz-area-activate\': isDetailShow}">' +
        '<div v-for="item in kind" class="quiz-content" :class="image()">{{ item }}</div>' +
        '</div>' +
        '<div v-show="isDetailShow">見えるようになります</div>' +
        '<div class="fotter-area">' +
        '</div>' +
        '</div>'
    ,methods: {
        detailShow: function()
        {
            if(this.isDetailShow){
                this.isDetailShow = false;
                this.detailText = '+';
                return;
            }

            this.isDetailShow = true;
            this.detailText = '-';
        }
    }
}

QUIZ.homeView =  new Vue({
        el: "#home-content",
        //pythonからjsonが下りてくるわけではないのでなんか考えないといけない
        data: {
            quiz: [
                {
                    name: 'new'
                    ,content: ['難しいやつ', '簡単なやつ']
                    ,sortNo: 0
                }
                ,
                {
                    name: 'javascript'
                    ,content: ['基本の基本の基本', 'クロージャとか', 'うんぬんかんぬん', 'うんぬんかんぬん', 'うんぬんかんぬん', 'うんぬんかんぬん', 'うんぬんかんぬん', 'うんぬんかんぬん', 'うんぬんかんぬん', 'うんぬんかんぬん', 'うんぬんかんぬん', 'うんぬんかんぬん']
                    ,sortNo: 8
                }
                ,{
                    name: 'css'
                    ,content: ['小技集オブザ小技', 'すさまじく基本', 'すごいやつ']
                    ,sortNo: 1
                }
            ]
        },
        components: {
            'content-area': QUIZ.homeComponent
        }
});