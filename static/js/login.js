var app2 = new Vue({
    el: '#login-section',
    watch: {
        email: function(newVal, oldVal) {
            this.error.email.init = true;
            this.error.email.require = (newVal.length < 1) ? true : false;
        },
        password: function(newVal, oldVal) {
            this.error.password.init = true;
            this.error.password.require= (newVal.length < 1) ? true : false;
            if(newVal == "" || newVal.length >= 8){
                this.error.password.tooShort = false;
                this.check();
            }else if(oldVal.length >= 8 && newVal.length < 8){
                this.error.password.tooShort = true;
                this.check();
            }
        }
    },
    methods: {
        emailFocus(e) {
            this.error.email.init = true;
            this.error.email.require = (e.target.value == "") ? true : false;
            this.check();
        },
        passwordFocus(e) {
            this.error.password.init = true;
            this.error.password.require = (e.target.value == "") ? true : false;
            this.error.password.tooShort = (e.target.value.length < 8 && e.target.value.length > 0) ? true : false;
            this.check();
        }
    },
    data: {
        message: 'Hello',
        email: '',
        password: '',
        error: {
            email: {
                init: false,
                require: true,
                tooLong: false
            },
            password: {
                init: false,
                require: true,
                tooShort: false
            },
            loginBtn: false
        },
        check: function(){
            if(!this.error.email.require && !this.error.password.require && !this.error.password.tooShort){
                this.error.loginBtn = true;
            }else{
                this.error.loginBtn = false;
            }
        }
    }
})
