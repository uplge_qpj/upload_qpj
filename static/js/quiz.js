var choicesRadio = {
    data: function () {
        return {
        }
    },
    methods: {
        qTypeChange(e) {
        },
        qRmv(e) {
        }
    },
    template: '<ul class="choices-radio"><li><input name="q-choices-radio" type="text" placeholder="問題の選択肢：ラジオボタン"><input class="q-choices-delete" type="button" value="削除"></li></ul>'
};
var choicesCheckbox = {
    data: function () {
        return {
        }
    },
    methods: {
        qTypeChange(e) {
        },
        qRmv(e) {
        }
    },
    template: '<ul class="choices-checkbox"><li><input name="q-choices-checkbox" type="checkbox" value="0"><input name="q-choices-checkbox" type="text" placeholder="問題の選択肢：チェックボックス"></li></ul>'
};
var descriptiveText = {
    data: function () {
        return {
            qDescriptiveText: ""
        }
    },
    template: '<ul class="descriptive-text"><li><input name="q-descriptive-text" type="text" placeholder="記述式の答え：テキスト" v-model="qDescriptiveText"></li></ul>'
};
var qTypeContents = {
    data: function () {
        return {
        }
    },
    props: {
        'flg': String,
    },
    components: {
        'choices-radio': choicesRadio,
        'choices-checkbox': choicesCheckbox,
        'descriptive-text': descriptiveText
    },
    methods: {
        qTypeChange(e) {
        },
        qRmv(index) {
        }
    },
    template: '<div><choices-radio v-if="flg==&quot;choices-radio&quot;"></choices-radio><choices-checkbox v-if="flg==&quot;choices-checkbox&quot;"></choices-checkbox><descriptive-text v-if="flg==&quot;descriptive-text&quot;"></descriptive-text></div>'
};






var qContent = {
    data: function () {
        return {
          count: 0,
          qDetail: "",
          selectedType: ""
        }
    },
    props: {
      'types': Array,
      'qIdx': Number,
      'qObj': Object
    },
    components: {
        'q-type-contents': qTypeContents
    },
    methods: {
        qDetailFocus(index, qid){
            console.log("blur");
            var updObj = {
                "id": qid,
                "qDetail": this.qDetail,
                "qType": this.selectedType
            };
            this.$root.qList.splice(index, 1, updObj)
        },
        qTypeChange(index, qid) {
            console.log("change");
            var updObj = {
                "id": qid,
                "qDetail": this.qDetail,
                "qType": this.selectedType
            };
            this.$root.qList.splice(index, 1, updObj)
        },
        qRmv(index) {
            console.log("click");
            this.$root.qList.splice(index, 1);
        }
    },
    mounted: function () {
        this.qDetail = this.qObj.qDetail;
        this.selectedType = this.qObj.qType;
    },
    template: '\
        <div class="q-content">\
            <textarea @blur="qDetailFocus(qIdx, qObj.id)" v-model.trim="qDetail" name="q-detail" maxlength="100" rows="4" placeholder="問題文：100文字以内"></textarea>\
            <div class="q-type">\
                <select @change="qTypeChange(qIdx, qObj.id)" v-model="selectedType" name="q-type">\
                    <option v-for="(elem, index) in types" :value="elem.value" :label="elem.label">\
                        {{ elem.label }}\
                    </option>\
                </select>\
                <q-type-contents :flg="selectedType"></q-type-contents>\
            </div>\
            <input @click="qRmv(qIdx)" v-if="this.$root.qList.length > 1" name="q-rmv" value="削除" type="button" style="float:right;">\
        </div>'
};

var main = new Vue({
    el: "#main",
    components: {
        'q-content': qContent,
    },
    data: {
        qId: 1,
        qTypes:[{"value":"", "label":"問題の種類を選択"},{"value":"choices-radio", "label":"選択：ラジオボタン"},{"value":"choices-checkbox", "label":"複数選択：チェックボックス"},{"value":"descriptive-text", "label":"記述式：テキスト"}],
        qList: [{"id":"q1", "qDetail":"test", "qType":"choices-radio"}]
    },
    methods: {
        qAdd(e) {
            console.log("click");
            this.qId++;
            var obj = {
                "id": "q" + this.qId,
                "qDetail": "",
                "qType": ""
            };
            this.qList.push(obj);
        }
    }
});
