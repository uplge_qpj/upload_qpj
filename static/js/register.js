var app2 = new Vue({
    el: '#new-register-section',
    watch: {
        email: function(newVal, oldVal) {
            this.error.email.init = true;
            this.error.email.require = (newVal.length < 1) ? true : false;
        },
        emailConfirm: function(newVal, oldVal) {
            this.error.emailConfirm.init = true;
            this.error.emailConfirm.require = (newVal.length < 1) ? true : false;
        },
        password: function(newVal, oldVal) {
            this.error.password.init = true;
            this.error.password.require= (newVal.length < 1) ? true : false;
            if(newVal == "" || newVal.length >= 8){
                this.error.password.tooShort = false;
                this.check();
            }else if(oldVal.length >= 8 && newVal.length < 8){
                this.error.password.tooShort = true;
                this.check();
            }
        },
        passwordConfirm: function(newVal, oldVal) {
            this.error.passwordConfirm.init = true;
            this.error.passwordConfirm.require= (newVal.length < 1) ? true : false;
            if(newVal == "" || newVal.length >= 8){
                this.error.passwordConfirm.tooShort = false;
                this.check();
            }else if(oldVal.length >= 8 && newVal.length < 8){
                this.error.passwordConfirm.tooShort = true;
                this.check();
            }
        }
    },
    methods: {
        emailFocus(e) {
            this.error.email.init = true;
            this.error.email.require = (e.target.value == "") ? true : false;
            this.check();
        },
        emailConfirmFocus(e) {
            this.error.emailConfirm.init = true;
            this.error.emailConfirm.require = (e.target.value == "") ? true : false;
            this.check();
        },
        passwordFocus(e) {
            this.error.password.init = true;
            this.error.password.require = (e.target.value == "") ? true : false;
            this.error.password.tooShort = (e.target.value.length < 8 && e.target.value.length > 0) ? true : false;
            this.check();
        },
        passwordConfirmFocus(e) {
            this.error.passwordConfirm.init = true;
            this.error.passwordConfirm.require = (e.target.value == "") ? true : false;
            this.error.passwordConfirm.tooShort = (e.target.value.length < 8 && e.target.value.length > 0) ? true : false;
            this.check();
        }
    },
    data: {
        message: 'Hello',
        email: '',
        emailConfirm: '',
        password: '',
        passwordConfirm: '',
        error: {
            email: {
                init: false,
                require: true,
                tooLong: false
            },
            emailConfirm: {
                init: false,
                require: true,
                tooLong: false
            },
            password: {
                init: false,
                require: true,
                tooShort: false
            },
            passwordConfirm: {
                init: false,
                require: true,
                tooShort: false
            },
            registerBtn: false
        },
        check: function(){
            if(!this.error.email.require && !this.error.password.require && !this.error.password.tooShort
                && !this.error.emailConfirm.require && !this.error.passwordConfirm.require && !this.error.passwordConfirm.tooShort){
                this.error.registerBtn = true;
            }else{
                this.error.registerBtn = false;
            }
        }
    }
})
