from django.apps import AppConfig


class SystemConfConfig(AppConfig):
    name = 'system_conf'
