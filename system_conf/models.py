from django.db import models


class URLSettings(models.Model):
    """フッターのURL設定"""
    setting_no = models.IntegerField(unique=True)
    URL = models.CharField(max_length=30000)
    URLName = models.CharField(max_length=200)
